package com.jonylima.DownloadImage.service;

import com.jonylima.DownloadImage.model.Imagem;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import javafx.concurrent.Task;

public class DowloadService extends Task {

    private final static char SEPARATOR = ',';
    private final Path caminho;
    private List<String> linhas;
    private Supplier supplierSucesso;
    private Consumer<Throwable> consumerError;

    public DowloadService(String arquivo) {
        this.caminho = Paths.get(arquivo);
    }

    private void validarArquivo() {
        try {
            linhas = Files.readAllLines(caminho);
            boolean validado = linhas.stream().allMatch(linha -> linha.matches("^.+" + SEPARATOR + ".+" + SEPARATOR + ".+$"));

            if (!validado) {
                throw new RuntimeException("Arquivo não está no padrão correto corrija-o e tente novamente");
            }
        } catch (IOException ex) {
            throw new RuntimeException("Não foi possivél ler o arquivo");
        }
    }

    private Imagem getInstance(String linha) {
        String array[] = linha.split(String.valueOf(SEPARATOR));
        return new Imagem(array[0], array[1], array[2]);
    }

    @Override
    protected Void call() throws Exception {
        try {
            validarArquivo();
            double size = linhas.size();
            int nrLinha = 0;

            for (String linha : linhas) {
                try (Imagem imagem = getInstance(linha)) {
                    imagem.salvarImagem();
                    nrLinha++;
                    updateProgress(nrLinha, size);
                }
            }

        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        return null;
    }

    @Override
    protected void succeeded() {
        if (supplierSucesso != null) {
            supplierSucesso.get();
        }
        updateProgress(0, 0);
    }

    @Override
    protected void failed() {
        if (consumerError != null) {
            consumerError.accept(getException());
        }
        updateProgress(0, 0);
    }

    public void seSucesso(Supplier supplierSucesso) {
        this.supplierSucesso = supplierSucesso;
    }

    public void seErro(Consumer<Throwable> consumerError) {
        this.consumerError = consumerError;
    }

}
