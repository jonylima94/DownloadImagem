/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jonylima.DownloadImage.view;

import java.io.IOException;
import java.net.URL;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    
    private static final URL URL_FXML = Main.class.getResource("DownloadFXML.fxml");
    
    @Override
    public void start(Stage stage) throws IOException {
        Parent parent = (Parent) FXMLLoader.load(URL_FXML);
        Scene scene = new Scene(parent);
        stage.setResizable(false);
        stage.setTitle("Download Imagem");
        stage.setScene(scene);
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }
    
}
