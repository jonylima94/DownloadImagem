package com.jonylima.DownloadImage.view;

import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;

public class ViewUtils {

    public static void exibirErro(String mensagem) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Informação de erro");
        alert.setHeaderText(null);
        alert.setContentText(mensagem);
        alert.showAndWait();
    }

    public static void exibirInformacao(String mensagem) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Informação");
        alert.setHeaderText(null);
        alert.setContentText(mensagem);
        alert.showAndWait();
    }

    public static boolean exibirConfirmacao(String cabecalho, String mensagem) {
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setTitle("Confirmação");
        alert.setHeaderText(cabecalho);
        alert.setContentText(mensagem);

        ButtonType sim = new ButtonType("Sim");
        ButtonType nao = new ButtonType("Não");

        alert.getButtonTypes().setAll(sim, nao);

        Optional<ButtonType> result = alert.showAndWait();
        return result.get() == sim;
    }
}
