package com.jonylima.DownloadImage.controller;

import com.jonylima.DownloadImage.service.DowloadService;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import com.jonylima.DownloadImage.view.ViewUtils;

public class DownloadController {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ProgressBar pbDownload;

    @FXML
    private Label lblNmPrograma;

    @FXML
    private TextField txfCaminhoArquivo;

    @FXML
    private Button btnSelecionarArquivo;

    @FXML
    private Label lblEmail;

    @FXML
    private Button btnBaixar;

    @FXML
    private Label lblNome;
    @FXML
    private Button btnLimpar;

    @FXML
    public void onActionBtnSlcArquivo(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        chooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File file = chooser.showOpenDialog(new Stage());

        if (file != null) {
            txfCaminhoArquivo.setText(file.getPath());
            btnBaixar.setDisable(false);
        }

    }

    @FXML
    public void onActionBtnBaixar(ActionEvent event) {
        if (ViewUtils.exibirConfirmacao("Caso exista arquivos com mesmos nomes dos baixados eles serão substituídos", "Deseja continuar?")) {
            desabilitarBotoes();
            DowloadService dowloadService = new DowloadService(txfCaminhoArquivo.getText());
            pbDownload.progressProperty().bind(dowloadService.progressProperty());

            dowloadService.seSucesso(() -> {
                ViewUtils.exibirInformacao("Imagens baixadas com sucesso");
                habilitarBotoes();
                return null;
            });

            dowloadService.seErro((t) -> {
                String mensagem = t.getMessage();
                mensagem = mensagem.substring(mensagem.indexOf(":") + 1).trim();
                ViewUtils.exibirErro(mensagem);
                habilitarBotoes();
            });

            Thread thread = new Thread(dowloadService);
            thread.setDaemon(true);
            thread.start();
        }
    }

    private void desabilitarBotoes() {
        btnBaixar.setDisable(true);
        btnSelecionarArquivo.setDisable(true);
        btnLimpar.setDisable(true);
    }

    private void habilitarBotoes() {
        btnBaixar.setDisable(false);
        btnSelecionarArquivo.setDisable(false);
        btnLimpar.setDisable(false);
    }

    @FXML
    public void onActionBtnLimpar(ActionEvent event) {
        txfCaminhoArquivo.setText(null);
        btnBaixar.setDisable(true);
    }
}
