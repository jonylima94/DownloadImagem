package com.jonylima.DownloadImage.model;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class Imagem implements Closeable {

    private InputStream inputStream;
    private Path path;

    public Imagem(String url, String localGravacao, String nomeArquivo) {
        try {

            HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
            inputStream = urlConnection.getInputStream();
            this.path = Paths.get(localGravacao, nomeArquivo + getExtensao(urlConnection));
        } catch (MalformedURLException e) {
            throw new RuntimeException("url inválida do arquivo " + nomeArquivo, e);
        } catch (IOException e) {
            throw new RuntimeException("Não foi possível realizar leitura da imagem " + nomeArquivo, e);
        }
    }

    private String getExtensao(HttpURLConnection urlConnection) {
        String contentType = urlConnection.getHeaderField("content-type");

        if (!contentType.startsWith("image/")) {
            throw new IllegalArgumentException("Url informada não é de um arquivo de imagem");
        }
        return contentType.replace("image/", ".");

    }

    public void salvarImagem() {
        try {
            Files.copy(inputStream, path, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            throw new RuntimeException("Não foi possivel salvar imagem " + path.getFileName().toString());
        }
    }

    public boolean existeArquivo() {
        return Files.exists(path);
    }

    public String getNomeArquivo() {
        return path.getFileName().toString();
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }

}
